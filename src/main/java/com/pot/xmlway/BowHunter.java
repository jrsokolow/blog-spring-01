package com.pot.xmlway;

/**
 * Created by sokoloj1 on 2015-11-13.
 */
public class BowHunter implements Hunter {

    int age;
    String firstName;

    public BowHunter(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public void hunt() {
        System.out.println(this.firstName + " is hunting via bow, age " + this.age);
    }

    public void setAge(int age) {
        this.age = age;
    }
}
