package com.pot;

import com.pot.xmlway.Hunter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class SpringPracticeApplication {

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("file:src/main/java/com/pot/config.xml");

        Hunter hunter = (Hunter) context.getBean("bowHunter");
        hunter.hunt();

        SpringApplication.run(SpringPracticeApplication.class, args);
    }
}
